import { ReactElement } from "react";

interface TabScreen {
  tab: ReactElement;
  tabState: ReactElement;
  name: string;
}

export default TabScreen;
